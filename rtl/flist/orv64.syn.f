-f common.syn.f
-f oursring.syn.f

../../rtl/orv64/orv64_define.sv
../../rtl/orv64/orv64_param_pkg.sv
../../rtl/orv64/orv64_typedef_pkg.sv
../../rtl/orv64/orv64_func_pkg.sv

../../rtl/orv64/orv64_oursring_if_arbiter.sv
../../rtl/orv64/orv64_ram.sv

../../rtl/orv64/orv64_perm_checker.sv
../../rtl/orv64/orv64_napot_addr.sv
../../rtl/orv64/orv64_clk_gating.sv
../../rtl/orv64/orv64_regfile.sv
../../rtl/orv64/orv64_int_regfile.sv
../../rtl/orv64/orv64_fp_regfile.sv
../../rtl/orv64/orv64_fetch.sv
../../rtl/orv64/orv64_decode_func_pkg.sv
../../rtl/orv64/orv64_decode.sv
../../rtl/orv64/orv64_alu.sv
../../rtl/orv64/orv64_mul.sv
../../rtl/orv64/orv64_div.sv
../../rtl/orv64/orv64_fp_add.sv
../../rtl/orv64/orv64_fp_mac.sv
../../rtl/orv64/orv64_fp_div.sv
../../rtl/orv64/orv64_fp_sqrt.sv
../../rtl/orv64/orv64_fp_cmp.sv
../../rtl/orv64/orv64_fp_misc.sv
../../rtl/orv64/orv64_execute.sv
../../rtl/orv64/orv64_mem_access.sv
../../rtl/orv64/orv64_edeleg_checker.sv
../../rtl/orv64/orv64_ideleg_checker.sv
../../rtl/orv64/orv64_csr.sv
../../rtl/orv64/orv64_rvc_inst_build.sv
../../rtl/orv64/orv64_icache.sv
../../rtl/orv64/orv64_icache_sysbus.sv
../../rtl/orv64/orv64_inst_buffer.sv
../../rtl/orv64/orv64_icache_top.sv
../../rtl/orv64/orv64_storebuf.sv
../../rtl/orv64/orv64_dcache_bypass.sv
../../rtl/orv64/orv64_breakpoint.sv
../../rtl/orv64/orv64_stall.sv
../../rtl/orv64/orv64_inst_trace_buf.sv
../../rtl/orv64/orv64_debug_access.sv
../../rtl/orv64/orv64_cache_noc.sv
../../rtl/orv64/orv64_tlb.sv
../../rtl/orv64/orv64_pmp_match.sv
../../rtl/orv64/orv64_pmp_checker.sv
../../rtl/orv64/orv64_ptw_core.sv
../../rtl/orv64/orv64_ptw.sv
../../rtl/orv64/orv64.sv
